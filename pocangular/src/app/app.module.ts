import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PedidosModule } from './pedidos/pedidos.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './template_components/header/header.component';
import { FooterComponent } from './template_components/footer/footer.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { StatusButtonComponent } from './template_components/status-button/status-button.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PedidoService } from './pedidos/pedido.service';
import { GeolocalizacaoComponent } from './pedidos/geolocalizacao/geolocalizacao.component';
import { FormularioCadastroEntregadorComponent } from './entregador/formulario-cadastro-entregador/formulario-cadastro-entregador.component';
import { AlertComponent } from './template_components/alert/alert.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GeolocalizacaoComponent,
    FormularioCadastroEntregadorComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PedidosModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MatToolbarModule,
    HttpClientModule
  ],
  providers: [
    PedidoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
