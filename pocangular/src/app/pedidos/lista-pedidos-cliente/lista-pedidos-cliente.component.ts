import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faMapMarkerAlt, faMap, faBoxOpen } from '@fortawesome/free-solid-svg-icons';
import { Observable, of } from 'rxjs';
import { PedidoItemComponent } from '../pedido-item/pedido-item.component';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Pedido } from '../../models/pedido';
import { PedidoService } from '../pedido.service';

@Component({
  selector: 'app-lista-pedidos-cliente',
  templateUrl: './lista-pedidos-cliente.component.html',
  styleUrls: ['./lista-pedidos-cliente.component.css']
})
export class ListaPedidosClienteComponent implements OnInit {

  currentlyClickedCardIndex = 0;
  currentlyClickedPedido = {} as Pedido;

  pedido = {} as Pedido;
  pedidos: Pedido[] = [];

  faMapMarkerAlt = faMapMarkerAlt;
  faMap = faMap;
  faBoxOpen = faBoxOpen;
  mensagemErro: any;
  cardselected: boolean = false;

  constructor(private router: Router, private route: ActivatedRoute, private service: PedidoService) {
    this.refresh();
    //this.currentlyClickedPedido = this.pedidos[0];
    this.currentlyClickedCardIndex = 0;
  }

  ngOnInit(): void {

  }

  public setcurrentlyClickedCardIndex(cardIndex: number): void {
    this.currentlyClickedCardIndex = cardIndex;
    this.currentlyClickedPedido = this.pedidos.find(x => x.id === this.currentlyClickedCardIndex)!;
  }

  public checkIfCardIsClicked(cardIndex: number): boolean {
    return cardIndex === this.currentlyClickedCardIndex;
  }

  public checkIfCardIsSelected(cardIndex: number): boolean {
    if( cardIndex == this.currentlyClickedCardIndex){
      this.cardselected = true;
    }else this.cardselected = false;

    return this.cardselected
  }


  public confirmPedido() {
    this.router.navigateByUrl('/pedidos-cliente/' + this.currentlyClickedPedido.id);
  }

  public refresh() {
    this.service.getPedidos().subscribe((pedidos: Pedido[]) => {
      this.pedidos = pedidos;
    });

  }

}
