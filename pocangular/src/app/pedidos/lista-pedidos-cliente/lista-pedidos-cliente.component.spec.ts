import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPedidosClienteComponent } from './lista-pedidos-cliente.component';

describe('ListaPedidosClienteComponent', () => {
  let component: ListaPedidosClienteComponent;
  let fixture: ComponentFixture<ListaPedidosClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaPedidosClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPedidosClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
