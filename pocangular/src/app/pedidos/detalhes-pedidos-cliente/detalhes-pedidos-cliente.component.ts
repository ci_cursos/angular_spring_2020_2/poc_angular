import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Endereco } from '../../models/endereco';
import { Pedido } from '../../models/pedido';
import { PedidoService } from '../pedido.service';
import { faMapMarkerAlt, faMap, faBoxOpen, faTag, faPeopleCarry, faClock, faMotorcycle, faDotCircle } from '@fortawesome/free-solid-svg-icons';
import { faWaze, faGoogle } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-detalhes-pedidos-cliente',
  templateUrl: './detalhes-pedidos-cliente.component.html',
  styleUrls: ['./detalhes-pedidos-cliente.component.css']
})
export class DetalhesPedidosClienteComponent implements OnInit {

  pedido = {} as Pedido;

  endereco_entrega = {} as Endereco;
  endereco_farmacia =  {} as Endereco;
  tempo_estimado? = "";

  id: number = -1;
  mensagemErro: any;

  faMapMarkerAlt = faMapMarkerAlt;
  faMap = faMap;
  faBoxOpen = faBoxOpen;
  faDotCircle = faDotCircle
  faTag = faTag;
  faWaze = faWaze;
  faGoogle= faGoogle;
  faPeopleCarry = faPeopleCarry
  faClock = faClock;
  faMotorcycle = faMotorcycle;

  mostrarRastreamento: boolean = false;

  constructor(private route: ActivatedRoute, private service: PedidoService) {
  }


  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.service
        .getPedidoPorId(this.id)
        .subscribe((pedido: Pedido) =>{
          console.log(pedido);
          this.pedido = pedido;
        } );
  }


  visualizarastreamento(){
     this.mostrarRastreamento = !this.mostrarRastreamento;

  }



}
