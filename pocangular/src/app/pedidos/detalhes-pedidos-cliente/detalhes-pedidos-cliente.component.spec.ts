import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalhesPedidosClienteComponent } from './detalhes-pedidos-cliente.component';

describe('DetalhesPedidosClienteComponent', () => {
  let component: DetalhesPedidosClienteComponent;
  let fixture: ComponentFixture<DetalhesPedidosClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalhesPedidosClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalhesPedidosClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
