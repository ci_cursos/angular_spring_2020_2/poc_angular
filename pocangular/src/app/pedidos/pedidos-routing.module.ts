import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetalhesPedidosClienteComponent } from './detalhes-pedidos-cliente/detalhes-pedidos-cliente.component';
import { ListaPedidosClienteComponent } from './lista-pedidos-cliente/lista-pedidos-cliente.component';
import { ListaPedidosComponent } from './lista-pedidos/lista-pedidos.component';
import { PedidoRetiradoComponent } from './pedido-retirado/pedido-retirado.component';
import { PedidosDetalhesComponent } from './pedidos-detalhes/pedidos-detalhes.component';
import { EntregadorComponent } from '../entregador/entregador.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'pedidos/entregador',
        children: [
          {
            path: ':id',
            component: ListaPedidosComponent
          },
        ]
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'processo-entrega',
        children: [
          {
            path: ':id',
            component: PedidosDetalhesComponent
          }
        ]
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'pedidos-cliente',
        children: [
          {
            path: '',
            component: ListaPedidosClienteComponent
          },
          {
            path: ':id',
            component: DetalhesPedidosClienteComponent
          }
        ]
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'painel-de-controle',
        children: [
          {
            path: '',
            component: DashboardComponent
          }
        ]
      }
    ]
  },
  {
    path: 'pedidos-retirado',
    children: [
      {
        path: ':id',
        component: PedidoRetiradoComponent
      },
    ]
  },
{
  path: 'acesso', component: EntregadorComponent
}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PedidosRoutingModule { }
