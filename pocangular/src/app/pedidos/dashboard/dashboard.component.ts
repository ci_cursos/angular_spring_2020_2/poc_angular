import { Component, OnInit } from '@angular/core';
import { Pedido } from 'src/app/models/pedido';
import { PedidoService } from '../pedido.service';
import { faArrowUp, faArrowDown, faBoxOpen, faMotorcycle, faClock, faSync } from '@fortawesome/free-solid-svg-icons';
import { Dashboard } from 'src/app/models/dashboard';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pedidos: Pedido[] = [];
  metricas = {} as Dashboard;
  faArrowUp = faArrowUp
  faArrowDown = faArrowDown
  faBoxOpen = faBoxOpen
  faMotorcycle = faMotorcycle
  faClock = faClock
  faSync = faSync

  constructor(private service: PedidoService) { }

  ngOnInit(): void {
    this.service.getAllPedidos().subscribe((pedidos: Pedido[]) => {
      this.pedidos = pedidos.filter((x: Pedido) => x.status == "ENTREGUE");
      console.log(this.pedidos);
    }, error => console.log(error)
    );

    this.service.getMetricas().subscribe((metricas: Dashboard) => {
      this.metricas = metricas;
    }, error => console.log(error)
    );
  }

  public refresh() {
    this.service.getAllPedidos().subscribe((pedidos: Pedido[]) => {
      this.pedidos = pedidos.filter((x: Pedido) => x.status == "ENTREGUE");
      console.log(this.pedidos);
    }, error => console.log(error)
    );

    this.service.getMetricas().subscribe((metricas: Dashboard) => {
      this.metricas = metricas;
    }, error => console.log(error)
    );
  }
}
