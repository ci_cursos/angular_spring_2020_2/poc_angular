import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoRetiradoComponent } from './pedido-retirado.component';

describe('PedidoRetiradoComponent', () => {
  let component: PedidoRetiradoComponent;
  let fixture: ComponentFixture<PedidoRetiradoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidoRetiradoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoRetiradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
