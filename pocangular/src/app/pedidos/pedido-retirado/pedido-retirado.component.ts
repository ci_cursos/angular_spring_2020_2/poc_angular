import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Endereco } from '../../models/endereco';
import { Pedido } from '../../models/pedido';
import { PedidoService } from '../pedido.service';
import { faMapMarkerAlt, faMap, faBoxOpen, faTag, faPeopleCarry, faClock, faMotorcycle } from '@fortawesome/free-solid-svg-icons';
import { faWaze, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { AlertService } from 'src/app/template_components/alert/alert.service';

@Component({
  selector: 'app-pedido-retirado',
  templateUrl: './pedido-retirado.component.html',
  styleUrls: ['./pedido-retirado.component.css']
})
export class PedidoRetiradoComponent implements OnInit {

  pedido = {} as Pedido;
  show: boolean = false;

  endereco_entrega = {} as Endereco;
  endereco_farmacia = {} as Endereco;
  tempo_estimado?= "";

  id: number = -1;
  mensagemErro: any;

  faMapMarkerAlt = faMapMarkerAlt;
  faMap = faMap;
  faBoxOpen = faBoxOpen;
  faTag = faTag;
  faWaze = faWaze;
  faGoogle = faGoogle;
  faPeopleCarry = faPeopleCarry
  faClock = faClock;
  faMotorcycle = faMotorcycle;

  mostrarRastreamento: boolean = false

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(private route: ActivatedRoute, private service: PedidoService, private router: Router, public alertService: AlertService) {
  }


  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.service
      .getPedidoPorId(this.id)
      .subscribe((pedido: Pedido) => {
        console.log(pedido);
        this.pedido = pedido;
      });
  }

  visualizarastreamento() {
    this.mostrarRastreamento = !this.mostrarRastreamento;
  }

  showModal() {
    this.show = !this.show;
  }

  returnHome() {
    this.confirmEntrega()
    
  }

  public confirmEntrega() {
    this.service.getPedidoPorId(this.id).subscribe((pedido: Pedido) => {
      this.pedido = pedido;
      this.pedido.status = "ENTREGUE"
      this.pedido.tempoDeFinalizacao = new Date()
      this.pedido.tempoDeEntrega = (this.pedido.tempoDeFinalizacao.getTime() - new Date(this.pedido.tempoInicial!).getTime()) / 60000
      this.service.updatePedido(this.pedido).subscribe(() => {
        this.pedido.entregador!.status = "DISPONÍVEL"
        this.service.updateEntregador(this.pedido.entregador!).subscribe(() => {
          this.router.navigateByUrl('/pedidos/entregador/' + this.pedido.entregador?.id);
          this.alertService.success('Pedido Entregue com Sucesso! &nbsp;&nbsp;', this.options)
        })
      })
    });
  }

}
