import { Component, OnInit } from '@angular/core';
import { Pedido } from '../../models/pedido';
import { PedidoService } from '../pedido.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-status-pedido',
  templateUrl: './status-pedido.component.html',
  styleUrls: ['./status-pedido.component.css']
})
export class StatusPedidoComponent implements OnInit {

  id: number = -1;
  pedido = {} as Pedido;
  constructor(private route: ActivatedRoute, private service: PedidoService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.service
        .getPedidoPorId(this.id)
        .subscribe((pedido: Pedido) =>{
          console.log(pedido);
          this.pedido = pedido;
        } );
  }


  converteStatusTela(): string{
    const status: string = this.pedido.status;
    switch(status){
      case 'CRIADO':
        return 'c2';
        break;
      case 'EM_SEPARACAO':
        return 'c2';
        break;
      case 'RECEBIDO':
        return 'c2';
        break;
      case 'EM_ROTA':
        return 'c3';
        break;
      case 'ENTREGUE':
        return 'c4';
        break;
      default:
        return 'C0'
    }
  }

}
