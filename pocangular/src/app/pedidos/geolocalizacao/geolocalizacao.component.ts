import { Component, OnInit } from '@angular/core';
import {ChangeDetectionStrategy } from '@angular/core';
import {GeolocationService} from '@ng-web-apis/geolocation';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-geolocalizacao',
  templateUrl: './geolocalizacao.component.html',
  styleUrls: ['./geolocalizacao.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GeolocalizacaoComponent implements OnInit {
  position: any | null = null;
  error: any | null = null;

  constructor(
      readonly geolocation$: GeolocationService,
  ) {}
  ngOnInit(): void {
    
  }

  getCurrentPosition() {
      this.geolocation$.pipe(take(1)).subscribe(
          position => {
              const longitude = position.coords.longitude;
              const latitude = position.coords.latitude;
              console.log(longitude);
              console.log(latitude);
          },
          error => {
              this.error = error;
              window.alert('Necessario permitir localização.');
          },
      );
  }
}
