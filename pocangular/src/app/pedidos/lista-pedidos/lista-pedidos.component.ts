import { Component, OnInit, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faMapMarkerAlt, faMap, faBoxOpen, faAngleDown, faSync } from '@fortawesome/free-solid-svg-icons';

import { Pedido } from '../../models/pedido';
import { PedidoService } from '../pedido.service';
import { Entregador } from 'src/app/models/entregador';

import { GeolocationService } from '@ng-web-apis/geolocation';
import { take } from 'rxjs/operators';
import { AlertService } from 'src/app/template_components/alert/alert.service';


@Component({
  selector: 'app-lista-pedidos',
  templateUrl: './lista-pedidos.component.html',
  styleUrls: ['./lista-pedidos.component.css']
})

export class ListaPedidosComponent implements OnInit {

  currentlyClickedCardIndex = 0;
  currentlyClickedPedido = {} as Pedido;

  id: number = -1;

  pedido = {} as Pedido;
  pedidos: Pedido[] = [];
  faMapMarkerAlt = faMapMarkerAlt;
  faMap = faMap;
  faSync = faSync;
  faAngleDown = faAngleDown;
  faBoxOpen = faBoxOpen;
  mensagemErro: any;
  position: any | null = null;
  error: any | null = null;
  entregador = {} as Entregador;
  loader_spinner = true;


  constructor(private router: Router, private route: ActivatedRoute, private service: PedidoService, readonly geolocation$: GeolocationService, public alertService: AlertService) {

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.getEntregador(this.id);
    this.currentlyClickedCardIndex = 0;
  }

  public setcurrentlyClickedCardIndex(cardIndex: number): void {
    console.log("PEDIDO CLICADO")
    console.log(cardIndex)
    this.currentlyClickedCardIndex = cardIndex;
    this.currentlyClickedPedido = this.pedidos.find(x => x.id === this.currentlyClickedCardIndex)!;
    console.log(this.currentlyClickedPedido)
    console.log("CLIENTE")
    console.log(this.currentlyClickedPedido.cliente.latitude)
    console.log(this.currentlyClickedPedido.cliente.longitude)
    console.log("LOJA")
    console.log(this.currentlyClickedPedido.loja.latitude)
    console.log(this.currentlyClickedPedido.loja.longitude)
  }

  public checkIfCardIsClicked(cardIndex: number): boolean {
    return cardIndex == this.currentlyClickedCardIndex;
  }

  public confirmPedido() {
    this.changePedidoEntregador()
  }

  public changePedidoEntregador() {
    this.service.getPedidoPorId(this.currentlyClickedCardIndex).subscribe((pedido: Pedido) =>{
      this.pedido = pedido;
      this.pedido.entregador = this.entregador
      this.pedido.status = "PRONTO_PARA_ENTREGA"
      let tempoEstimado = (document.getElementById("time") as HTMLElement).innerHTML
      this.pedido.tempoEstimado = parseInt(tempoEstimado)
      this.pedido.tempoInicial = new Date()
      this.service.updatePedido(this.pedido).subscribe(() => {
        this.entregador.status = "EM_ROTA_DE_RETIRADA"
        this.service.updateEntregador(this.entregador).subscribe(() => {
          this.router.navigateByUrl('/processo-entrega/' + this.currentlyClickedPedido.id);
         })
      })
    } );
  }

  public async getEntregador(id: number) {
    this.service
      .getEntregadorPorId(id)
      .subscribe((entregador: Entregador) => {
        this.entregador = entregador;
        this.getCurrentPosition();
        console.log(this.entregador);
        this.refresh();
      });
  }

  public refresh() {
    this.loader_spinner = true;
    this.service.updateEntregador(this.entregador).subscribe(() => {
      this.loader_spinner = false;
      if (this.entregador.status == "DISPONÍVEL") {
        this.service.getPedidos().subscribe((pedidos: Pedido[]) => {
          this.pedidos = pedidos;
          console.log(this.pedidos);
        }, error => console.log(error)
        );
      }
    }, error => console.log(error)
    );

  }

  getCurrentPosition() {
    this.geolocation$.pipe(take(1)).subscribe(
      position => {
        this.entregador.latitude = position.coords.latitude;
        this.entregador.longitude = position.coords.longitude;
      },
      error => {
        this.error = error;
        window.alert('Necessario permitir localização.');
      },
    );
  }


}
