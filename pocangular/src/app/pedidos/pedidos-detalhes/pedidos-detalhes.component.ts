import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Endereco } from '../../models/endereco';
import { Pedido } from '../../models/pedido';
import { PedidoService } from '../pedido.service';
import { faMapMarkerAlt, faMap, faBoxOpen, faTag, faPeopleCarry, faClock, faMotorcycle } from '@fortawesome/free-solid-svg-icons';
import { faWaze, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { GeolocationService } from '@ng-web-apis/geolocation';

@Component({
  selector: 'app-pedidos-detalhes',
  templateUrl: './pedidos-detalhes.component.html',
  styleUrls: ['./pedidos-detalhes.component.css']
})
export class PedidosDetalhesComponent implements OnInit {

  pedido = {} as Pedido;

  endereco_entrega = {} as Endereco;
  endereco_farmacia =  {} as Endereco;
  tempo_estimado? = "";

  id: number = -1;
  mensagemErro: any;

  faMapMarkerAlt = faMapMarkerAlt;
  faMap = faMap;
  faBoxOpen = faBoxOpen;
  faTag = faTag;
  faWaze = faWaze;
  faGoogle= faGoogle;
  faPeopleCarry = faPeopleCarry
  faClock = faClock;
  faMotorcycle = faMotorcycle;

  mostrarRastreamento: boolean = false

  constructor(private router: Router, private route: ActivatedRoute, private service: PedidoService, readonly geolocation$: GeolocationService) {

  }


  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.service
        .getPedidoPorId(this.id)
        .subscribe((pedido: Pedido) =>{
          console.log(pedido);
          this.pedido = pedido;
        } );
  }

  public confirmRetirada() {
    this.changeStatusEntregador()
  }

  public changeStatusEntregador() {
    this.service.getPedidoPorId(this.id).subscribe((pedido: Pedido) =>{
      this.pedido = pedido;
      this.pedido.tempoDeRetirada = new Date()
      this.pedido.status = "EM_ROTA"
      this.service.updatePedido(this.pedido).subscribe(() => {
        this.pedido.entregador!.status = "EM_ROTA_DE_ENTREGA"
        this.service.updateEntregador(this.pedido.entregador!).subscribe(() => {
          this.router.navigateByUrl('/pedidos-retirado/' + this.id);
         })
      })
    } );
  }

  visualizarastreamento(){
     this.mostrarRastreamento = !this.mostrarRastreamento;
  }

}
