import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PedidosRoutingModule } from './pedidos-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PedidoItemComponent } from './pedido-item/pedido-item.component';
import { ListaPedidosComponent } from './lista-pedidos/lista-pedidos.component';
import { PedidosDetalhesComponent } from './pedidos-detalhes/pedidos-detalhes.component';
import { MapaComponent } from './mapa/mapa.component';
import { AgmCoreModule } from '@agm/core';
import { DirectionsMapDirective } from './directions-map.directive';
import { PedidoService } from './pedido.service';
import { StatusPedidoComponent } from './status-pedido/status-pedido.component';
import { DetalhesPedidosClienteComponent } from './detalhes-pedidos-cliente/detalhes-pedidos-cliente.component';
import { ListaPedidosClienteComponent } from './lista-pedidos-cliente/lista-pedidos-cliente.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PedidoRetiradoComponent } from './pedido-retirado/pedido-retirado.component';
import { StatusButtonComponent } from '../template_components/status-button/status-button.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { EntregadorComponent } from '../entregador/entregador.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AlertComponent } from '../template_components/alert/alert.component';



@NgModule({
  declarations: [
    DirectionsMapDirective,
    ListaPedidosComponent,
    PedidosDetalhesComponent,
    PedidoItemComponent,
    MapaComponent,
    StatusPedidoComponent,
    DetalhesPedidosClienteComponent,
    ListaPedidosClienteComponent,
    DashboardComponent,
    PedidoRetiradoComponent,
    StatusButtonComponent,
    EntregadorComponent,
    ],
  imports: [
    CommonModule,
    PedidosRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    AgmCoreModule.forRoot({
      //MAX KEY
     apiKey: 'AIzaSyAConoluOqDKzu_T6UHKlX8-7eE_Ab3UDA'
      //apiKey: "teste"
    })
  ],
  providers: [
    PedidoService
  ]
})

export class PedidosModule { }
