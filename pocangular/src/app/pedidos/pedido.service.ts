import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { Pedido } from '../models/pedido';
import { catchError, retry, tap } from 'rxjs/operators'
import { Entregador } from '../models/entregador';
import { Dashboard } from '../models/dashboard';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  private api = environment.api;

  constructor(private http: HttpClient) {

  }

  httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json') }

  // OK TEST E PROD
  public getEntregadorPorId(id: number): Observable<Entregador> {
    return this.http.get<Entregador>(`${this.api}/entregadores/${id}`).pipe(retry(2), catchError(this.handleError));
  }

  public getPedidos(): Observable<Pedido[]> {
    return this.http.get<Pedido[]>(`${this.api}/pedidos/disponiveis`).pipe(
      tap(dados => console.log('Retornando todos os pedidos'),catchError(this.handleError))
    );
  }

  public getAllPedidos(): Observable<Pedido[]> {
    return this.http.get<Pedido[]>(`${this.api}/pedidos/`).pipe(
      tap(dados => console.log('Retornando todos os pedidos'),catchError(this.handleError))
    );
  }

  public getPedidoPorId(id: number): Observable<Pedido> {
    return this.http.get<Pedido>(`${this.api}/pedidos/${id}`).pipe(retry(2), catchError(this.handleError))
  }

  // OK PROD
  public updateEntregador(entregador: Entregador): Observable<any> {
    return this.http.put(`${this.api}/entregadores/`, entregador, this.httpOptions).pipe(
      tap(_ => console.log(`atualizando entregador id=${entregador.id}`)),
      catchError(this.handleError())
    );
  }

  // OK PROD
  public updatePedido(pedido: Pedido): Observable<any> {
    return this.http.put(`${this.api}/pedidos/`, pedido, this.httpOptions).pipe(
      tap(_ => console.log(`atualizando entregador id=${pedido.id}`)),
      catchError(this.handleError())
    );
  }

   // OK PROD
   public getMetricas(): Observable<any> {
    return this.http.get<Dashboard>(`${this.api}/pedidos/dashboard/`).pipe(retry(2), catchError(this.handleError))
  }

  // OK TESTE
  //public updateStatus(entregador: Entregador): Observable<any> {
    //return this.http.put(`${this.api}/entregadores/${entregador.id}`, entregador, this.httpOptions).pipe(
      //tap(_ => console.log(`atualizando entregador id=${entregador.id}`)),
      //catchError(this.handleError())
   // );
  //}

  //public updatePedido(pedido: Pedido): Observable<any> {
    //console.log("ENTROU NO UPDATE")
    //return this.http.put(`${this.api}/pedidos/${pedido.id}`, pedido, this.httpOptions).pipe(
      //tap(_ => console.log(`atualizando pedido id=${pedido.id}`)),
      //catchError(this.handleError())
    //);
  //}

  public handleError() {
    console.log("DEU ERRO")
    return (erro: HttpErrorResponse) => {
      let msg = '';
      if (erro.error instanceof ErrorEvent) {
        //falha no clado cliente
        console.log(erro.error.message)
        msg = `Falha no cliente: ${erro.error.message}`;
      } else {
        //falha no lado servidor
        console.log(erro.message)
        msg = `Falha no servidor: ${erro.status}, ${erro.statusText}, ${erro.message}`;
      }
      console.log("ERRO => " + msg);
      return throwError(msg);
    }
  }

}
