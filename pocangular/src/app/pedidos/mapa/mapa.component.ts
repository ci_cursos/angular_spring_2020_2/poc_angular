import { Component, OnInit, Input } from '@angular/core';
import { ILatLng } from '../directions-map.directive';
import { Pedido } from '../../models/pedido';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  @Input() latitudeOrigin: number;
  @Input() longitudeOrigin: number;
  @Input() latitudeDestination: number;
  @Input() longitudeDestination: number;
  @Input() latitudeWaypoint : number;
  @Input() longitudeWaypoint : number;

  displayDirections = true;
  zoom = 15;

  constructor() {
    this.latitudeOrigin = 0;
    this.longitudeOrigin = 0;
    this.latitudeDestination = 0;
    this.longitudeDestination = 0;
    this.latitudeWaypoint = 0;
    this.longitudeWaypoint = 0;
  }

  ngOnInit(): void {
  }

}
