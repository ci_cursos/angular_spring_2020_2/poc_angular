import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntregadorComponent } from './entregador/entregador.component';
import { FormularioCadastroEntregadorComponent } from './entregador/formulario-cadastro-entregador/formulario-cadastro-entregador.component';

const routes: Routes = [
  {path: 'formulario', component: FormularioCadastroEntregadorComponent}

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
