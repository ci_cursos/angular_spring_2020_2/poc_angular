
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Entregador } from 'src/app/models/entregador';
import { PedidoService } from 'src/app/pedidos/pedido.service';

@Component({
  selector: 'app-status-button',
  templateUrl: './status-button.component.html',
  styleUrls: ['./status-button.component.css']
})
export class StatusButtonComponent implements OnInit {

  id: number = -1;

  color = 'color';
  pedido: any;
  entregador = {} as Entregador;
  check: boolean = false;

  constructor(private router: Router, private route: ActivatedRoute, private service: PedidoService) {

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getEntregador(this.id);
  }

  changeStatus() {
    if (this.entregador.status == "DISPONÍVEL") {
      this.check = false
      this.entregador.status = "INDISPONÍVEL"
      this.service.updateEntregador(this.entregador).subscribe(() => {
        this.check = false;
        window.location.reload();
      })

    } else {
      this.check = true
      this.entregador.status = "DISPONÍVEL"
      this.service.updateEntregador(this.entregador).subscribe(() => {
        this.check = true;
        window.location.reload();
      });

    }
  }

  public getEntregador(id: number) {
    this.service
      .getEntregadorPorId(id)
      .subscribe((entregador: Entregador) => {
        console.log(entregador);
        this.entregador = entregador;
      });
  };
}
