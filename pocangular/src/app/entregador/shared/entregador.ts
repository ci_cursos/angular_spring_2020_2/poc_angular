export class Entregador {

  /* DADOS PESSOAIS: */
  nomecompleto: string = '';
  cpf: string = '';
  dataNascimento: string = '';
  cnh: string = '';
  email: string = '';
  telefone: string = '';
  genero: number = 1;

  /* DADOS DE ENDEREÇO: */
  logradouro: string = '';
  numero: number = 1;
  bairro: string = '';
  cep: string = '';
  cidade: string = '';
  estado: number = 1;
  pais: number = 1;
}
