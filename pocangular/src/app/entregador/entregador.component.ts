import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Entregador } from './shared/entregador';
import { HttpClient } from '@angular/common/http';





@Component({
  selector: 'app-entregador',
  templateUrl: './entregador.component.html',
  styleUrls: ['./entregador.component.css']
})
export class EntregadorComponent implements OnInit {


  formEntregador = new FormGroup({});
  constructor(

    private formBuilder: FormBuilder,
    private http: HttpClient,


    ) { }

  ngOnInit(): void {
    this.createForm(new Entregador());
    this.onSunmit();

  }

  createForm(entregador: Entregador) {
    this.formEntregador = this.formBuilder.group({
      nomecompleto: [entregador.nomecompleto],
      cpf: [entregador.cpf],
      genero: [entregador.genero],
      dataNascimento: [entregador.dataNascimento],
      cnh: [entregador.cnh],
      email: [entregador.email],
      telefone: [entregador.telefone],
      logradouro: [entregador.logradouro],
      numero: [entregador.numero],
      bairro: [entregador.bairro],
      cep: [entregador.cep],
      cidade: [entregador.cidade],
      estado: [entregador.estado],
      pais: [entregador.pais]
    })

  }

 onSunmit (){
console.log("entrou");
const signUpButton = window.document.getElementById('signUp')!;
const signInButton = window.document.getElementById('signIn')!;
const container = window.document.getElementById('container')!;

signUpButton.addEventListener('click', () => {
    container.classList.add('right-panel-active');
});

signInButton.addEventListener('click', () => {
    container.classList.remove('right-panel-active');
});

  }


}
