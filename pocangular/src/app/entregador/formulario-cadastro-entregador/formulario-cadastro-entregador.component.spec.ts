import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCadastroEntregadorComponent } from './formulario-cadastro-entregador.component';

describe('FormularioCadastroEntregadorComponent', () => {
  let component: FormularioCadastroEntregadorComponent;
  let fixture: ComponentFixture<FormularioCadastroEntregadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioCadastroEntregadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCadastroEntregadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
