export interface Dashboard {
  distanciaMedia: number,
  maiorDistancia: number,
  maiorTempoDeEntrega: number,
  menorDistancia: number,
  menorTempoDeEntrega: number,
  numeroDePedidosHoje: number,
  numeroTotalDePedidos: number,
  tempoDeEntregaMedio: number
}
