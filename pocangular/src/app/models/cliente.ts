export interface Cliente {
    id: number
    nome: string,
    cep: string,
    logradouro: string,
    complemento: string,
    bairro: string,
    localidade: string,
    uf: string,
    latitude: number,
    longitude: number
}