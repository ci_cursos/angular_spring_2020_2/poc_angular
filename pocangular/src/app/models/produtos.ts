export interface Pedido {
    id: number,
    nome: string,
    valor: number,
    desconto: number,
    quantidade: number
}