import { Cliente } from "./cliente";
import { Endereco } from "./endereco";
import { Entregador } from "./entregador";
import { Loja } from "./loja";

export interface Pedido{
    id: number
    loja: Loja,
    cliente: Cliente,
    enderecoFarmacia: Endereco
    enderecoEntrega: Endereco
    status: string
    entregador?: Entregador
    tempoEstimado?: number
    tempoInicial?: Date
    tempoDeRetirada: Date
    tempoDeFinalizacao: Date
    tempoDeEntrega:  number
    distancia: number
}
