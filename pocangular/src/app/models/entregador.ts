import { Endereco } from "./endereco";
import { Pedido } from "./pedido";

export interface Entregador{
  id: number,
  nome: string,
  telefone: string,
  status: string,
  latitude: number,
  longitude: number,
  }
