export interface Loja {
    id: number,
    cep: string,
    logradouro: string,
    complemento: string,
    bairro: string,
    localidade: string,
    uf: string,
    latitude: number,
    longitude: number

}
